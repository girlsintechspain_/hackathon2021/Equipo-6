# Lugar seguro Web app

## Quick start

### Running the backend (in Docker)

```sh
make build-backend
make run-backend
# will be available at localhost:8080
```

For more specific details on the backend see [here](./backend/README.md)

### Running the frontend (in Docker)

```sh
make build-frontend
make run-frontend
# will be available at localhost:3000
```

For more specific details on the frontend see [here](./frontend/README.md)

## Tasks list:
- [ ] Implement handling errors
- [ ] Create error messages (design team)
- [ ] Implement testing
- [ ] Create a database connection
- [ ] Implement Login in/out
- [ ] Implement Sign up
- [ ] Create a feature to change the language (spa/eng)
- [ ] Implement Search
