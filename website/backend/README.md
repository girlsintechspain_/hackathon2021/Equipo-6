# Backend Code base

## Setting up the virtual environment

If you are running the code locally, and not inside Docker we reccomend creating a virtual environment for this project.

```sh
# create virtual env (will take your local python version)
python3 -m venv hacking-for-humanity

# run virtual env
source hacking-for-humanity/bin/activate
```

## FastAPI application (backend)

### Install requirements

```sh
pip install -r requirements.txt
```

Files:
 * [app/](app/)


To start the application and make it visible inside your network,
you need to run from inside of the app directory:

```
uvicorn app.main:app --reload --port 8080 
```

Then, open your favorite browser to visit your website at `localhost:8080`

### Linting your code

We are linting the project with [Black](https://github.com/psf/black) and [Flake8](https://flake8.pycqa.org/en/latest/) we reccomend running these both locally before pushing code as they are enforced in the github actions.
