from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse, PlainTextResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.testclient import TestClient

from .helpers import openfile

app = FastAPI()
templates = Jinja2Templates(directory="templates")
app.mount("/static", StaticFiles(directory="static"), name="static")


# root get for the home page
@app.get("/", response_class=HTMLResponse)
async def home(request: Request):
    data = {
        "text": "Home page",
        "title": "Pagina principal",
    }
    return templates.TemplateResponse("page.html", {"request": request, "data": data})


# to navigate between pages
@app.get("/page/{page_name}", response_class=HTMLResponse)
async def page(request: Request, page_name: str):
    data = {"page": page_name}
    return templates.TemplateResponse("page.html", {"request": request, "data": data})


# information for populate the blog
@app.get("/blog", response_class=HTMLResponse)
async def blog(request: Request):
    data = {
        "text": "blog",
        "title": "Pagina Blog",
        "posts": [
            {
                "title": "Poems selection",
                "author": "maria",
                "content": openfile("about.md"),
            },
            {
                "title": "Quotes selection",
                "author": "jose",
                "content": openfile("info.md"),
            },
        ],
    }
    return templates.TemplateResponse("blog.html", {"request": request, "data": data})


# information for populate the news section
@app.get("/news", response_class=HTMLResponse)
async def news(request: Request):
    data = {
        "items": [1, 3, 5, "perro", "gato"],
        "title": "News",
    }
    return templates.TemplateResponse("news.html", {"request": request, "data": data})


# information for populate the help section
@app.get("/ayuda", response_class=HTMLResponse)
async def news(request: Request):
    data = {
        "img": "add-image",
    }
    return templates.TemplateResponse("ayuda.html", {"request": request, "data": data})


# information for populate the support group section
@app.get("/soporte", response_class=HTMLResponse)
async def news(request: Request):
    data = {
        "img": "static/images/stronger3.png",
    }
    return templates.TemplateResponse(
        "soporte.html", {"request": request, "data": data}
    )


# information for populate the experiencia section
@app.get("/experiencia", response_class=HTMLResponse)
async def news(request: Request):
    data = {
        "text": "Comparte tu experiencia",
    }
    return templates.TemplateResponse(
        "experiencia.html", {"request": request, "data": data}
    )
